import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class DataService {

  url: string = "https://jsonplaceholder.typicode.com/albums"
  
  constructor(public elenco: HttpClient) {}

  getAll(){

    return this.elenco.get(this.url);

  }
}
