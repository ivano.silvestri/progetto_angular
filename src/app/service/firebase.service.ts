import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  url: string = "https://amcrud-cfc5e-default-rtdb.europe-west1.firebasedatabase.app/"

  constructor(public database: HttpClient) {}

  getAll(){

    return this.database.get(this.url);

  }
}
