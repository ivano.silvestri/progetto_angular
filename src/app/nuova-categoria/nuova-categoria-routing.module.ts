import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuovaCategoriaComponent } from './nuova-categoria.component';

const routes: Routes = [{ path: '', component: NuovaCategoriaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuovaCategoriaRoutingModule { }
