import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuova-categoria',
  templateUrl: './nuova-categoria.component.html',
  styleUrls: ['./nuova-categoria.component.css']
})
export class NuovaCategoriaComponent implements OnInit {

  AggiungiForm: FormGroup = new FormGroup({

    nome_categoria: new FormControl ('',  [Validators.required]),
    id_categoria: new FormControl ('',  [Validators.required]),
    descrizione: new FormControl ('',  [Validators.required])

  });
  
  constructor(private router: Router) { }

  ngOnInit(): void {}

  onSubmit(){ 

    console.log(this.AggiungiForm.value);

  }

  btAnnulla(){
   this.router.navigate(['../', 'categoria']);

  }

}
