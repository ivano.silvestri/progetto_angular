import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtentiRoutingModule } from './utenti-routing.module';
import { UtentiComponent } from './utenti.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';

import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';

import { environment } from '../../environments/environment';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';





@NgModule({
  declarations: [
    UtentiComponent
  ],
  imports: [
    CommonModule,
    UtentiRoutingModule,
    MatMenuModule,
    MatCardModule,
    MatButtonModule, //per componenti button
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatFormFieldModule,
    provideFirebaseApp(() => initializeApp({appId:'1:29981028444:web:fa36ef2b5d0bc7837a7699'})),
    provideFirestore(() => getFirestore()),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule

  ]
})
export class UtentiModule { }
