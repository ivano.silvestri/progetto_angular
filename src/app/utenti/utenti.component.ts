import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-utenti',
  templateUrl: './utenti.component.html',
  styleUrls: ['./utenti.component.css']
})
export class UtentiComponent implements OnInit {


  items: Observable<any[]>;

  displayedColumns = ['ID', 'Cognome', 'Nome', 'Telefono', 'email'] ;

  constructor(
    private httpClient: HttpClient,
    private router:Router,
    private firestore: AngularFirestore

    ) { 

      this.items = firestore.collection('utenti').valueChanges();

    }


  ngOnInit(): void {
  }

  apriNuovoUtente(){
    console.log("funzione priNuovoUtente chiamata!");
    this.router.navigate(['../', 'nuovo_utente']);
};

}
