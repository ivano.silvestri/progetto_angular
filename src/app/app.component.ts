import { Component } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Observable, subscribeOn } from 'rxjs';
import { DataService } from './service/data.service'
import { FirebaseService } from './service/firebase.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Progetto_Angular';

  private _userData: any = {};
  public get userData(): any {
    return this._userData;
  }
  public set userData(value: any) {
    this._userData = value;
  }
  //dati$: Observable <any>;
  dati$: any = {};
  lista_completa: any;
  displayedColumns = ['UseriD', 'id', 'title', 'modifica'];

/*   constructor(public datiService: DataService, public datiFirebse: FirebaseService) {

    //this.datiService.getAll().subscribe();
    this.datiService.getAll().subscribe(res =>

      this.lista_completa = res)

  } */

  constructor(public datiFirebse: FirebaseService) {

    //this.datiService.getAll().subscribe();
    this.datiFirebse.getAll().subscribe(res =>

      this.lista_completa = res)

  }

  getData(data: NgForm) {
    console.warn(data) //solo per debug del codice
    this.userData = data
  }




}
