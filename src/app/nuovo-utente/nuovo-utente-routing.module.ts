import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuovoUtenteComponent } from './nuovo-utente.component';

const routes: Routes = [{ path: '', component: NuovoUtenteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuovoUtenteRoutingModule { }
