import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

export interface Item { nome: string,
                        cognome: string,
                        email: string,
                        telefono: string; }

@Component({
  selector: 'app-nuovo-utente',
  templateUrl: './nuovo-utente.component.html',
  styleUrls: ['./nuovo-utente.component.css']
})
export class NuovoUtenteComponent implements OnInit {

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  AggiungiForm: FormGroup = new FormGroup({

    nome: new FormControl ('',  [Validators.required]),
    cognome: new FormControl ('',  [Validators.required]),
    email: new FormControl ('',  [Validators.required, Validators.email]),
    telefono: new FormControl ('')

  });

  constructor( private router: Router,
               private httpClient: HttpClient, 
               private afs: AngularFirestore) {
               
                this.itemsCollection = afs.collection<Item>('utenti'); 
                this.items = this.itemsCollection.valueChanges();
                }

  ngOnInit(): void {}


  addItem() {
    this.itemsCollection.add(this.AggiungiForm.value);    
    this.AggiungiForm.reset();
    this.router.navigate(['../', 'utenti']);

  }
  
  btAnnulla(){
    this.router.navigate(['../', 'utenti']);

  }

 
}
