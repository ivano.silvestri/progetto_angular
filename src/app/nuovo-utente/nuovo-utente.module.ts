import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuovoUtenteRoutingModule } from './nuovo-utente-routing.module';
import { NuovoUtenteComponent } from './nuovo-utente.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  declarations: [
    NuovoUtenteComponent
  ],
  imports: [
    CommonModule,
    NuovoUtenteRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule, //per componenti button
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatFormFieldModule
  ]
})
export class NuovoUtenteModule { }
