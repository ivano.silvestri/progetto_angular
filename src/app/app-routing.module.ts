import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavigationComponent } from './navigation/navigation.component';

const routes: Routes = [
  {
    path : '',
    component: DashboardComponent,
  },
  { path: 'nuovo_utente', loadChildren: () => import('./nuovo-utente/nuovo-utente.module').then(m => m.NuovoUtenteModule) },
  { path: 'nuova_categoria', loadChildren: () => import('./nuova-categoria/nuova-categoria.module').then(m => m.NuovaCategoriaModule) },
  { path: 'utenti', loadChildren: () => import('./utenti/utenti.module').then(m => m.UtentiModule) },
  {
    path : '**',
    component: DashboardComponent,
  },





];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
