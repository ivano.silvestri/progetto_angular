// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'progetto-crud',
    appId: '1:29981028444:web:fa36ef2b5d0bc7837a7699',
    storageBucket: 'progetto-crud.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyAJrt2yV5jh1irxkBBHGmpvWHDTjPUa-tE',
    authDomain: 'progetto-crud.firebaseapp.com',
    messagingSenderId: '29981028444',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
